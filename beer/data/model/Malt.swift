//
//  Malt.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class Malt: Codable {
    let name: String? = ""
    let amount: Unity?
}
