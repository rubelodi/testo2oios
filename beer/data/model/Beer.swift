//
//  Beer.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class Beer: Codable {
    var id: Int? = 0
    var name: String? = ""
    var tagline: String? = ""
    var first_brewed : String? = ""
    var description : String? = ""
    var image_url : String? = ""
    var abv : Double?
    var ibu : Double?
    var target_fg : Double?
    var target_og : Double?
    var ebc : Double?
    var srm : Double?
    var ph : Double?
    var attenuation_level : Double?
    var volume : Volume?
    var boil_volume : Volume?
    var method : Method?
    var ingredients : Ingredient?
    var food_pairing : [String]? = []
    var brewers_tips : String? = ""
    var contributed_by : String? = ""
}
