//
//  Volume.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class Volume: Codable {
    
    var value : Int? = 0
    var unit : String? = ""
}
