//
//  Constants.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import UIKit

// BASE URL OF SERVICES
let BASE_URL = "https://api.punkapi.com/v2/"

// Size of paginatiOn
let PAGINATION_SIZE = 25
let ORANGE_COLOR = UIColor("#CF9330")

// Actions of list of beers
public enum ActionsViewListBeers {
    case beers_loaded
}

public enum ErrorsViewListBeers {
    case generic
}
