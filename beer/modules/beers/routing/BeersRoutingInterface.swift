//
//  BeersRoutingInterface.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import UIKit

protocol BeersRoutingInterface {
    func initializeModule() -> BeerListViewController
    
    func goToDetail(beer : Beer, nav : UINavigationController?)
    func goBackNav(controller : UIViewController)
}
