//
//  BeerListViewInterface.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

protocol BeerListViewInterface {
    
    func postAction(_ action : ActionsViewListBeers, params : Any?)
    func showError(_ error : ErrorsViewListBeers)
}
