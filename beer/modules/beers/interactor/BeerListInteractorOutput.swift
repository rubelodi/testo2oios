//
//  BeerListInteractorOutput.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

public enum BeerListOutputActions {
    case getBeersLoaded
}

public enum BeerListOutputErrors {
    case getBeersLoadedError
}


protocol BeerListInteractorOutput {
    
    func processAction(_ type : BeerListOutputActions, params : Any?)
    func processError(_ type : BeerListOutputErrors, code : Any?)
}
